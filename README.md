# INTRODUCTION

Dans cette SAE nous devons mettre en place un environnement de travail permettant de réaliser les différents travaux en cours 
(python, SQL, java, bash, web, ..), capable d'évoluer pour inclure de nouveaux besoins.\
Dans notre cas nous installerons XUbuntu, le même OS que à l'IUT.

# PREMIER ESSAI : WSL2

Mon premier choix fut d'installer WSL2 "Windows Subsystem for Linux 2".
Si j'avais choisis cette option c'était pour le coté pratique de la chose, étant déjà habitué à utiliser un virtual host, WSL2 me semblait être une option qui s'en rapprochais. De plus il n'y avait pas le côté "invasif" du dual-boot, qui demande de choisir entre booter sur Windows ou sur XUbuntu à chaque démarrage.\
Afin de l'installer j'avais suivis [ce tutoriel](https://www.it-connect.fr/installer-wsl-2-sur-windows-10/).\
\
Ainsi, j'ai pu arriver jusqu'à la fin de l'installation, mais lorsque j'ai essayé de lancer Ubuntu.. Le programme ne se lance pas.. puis après un peu d'attente, un message d'erreure apparait.\
![Erreure](https://gitlab.com/thomas.loque/sae-systeme-1/-/raw/main/Sources/Capture.PNG)\
\
J'ai donc du choisir une autre option..
# OPTION CHOiSIE : UN DUAL-BOOT

En effet une des autre option qui s'offrait à moi était le dual boot.\
J'ai donc choisi cette option, qui me permet d'avoir de la performence, contrairement à une machine virtuelle.\
Pour ce faire, j'ai récupéré les fichiers de XUbuntu sur ma clef USB (Nous n'avions pas encore reçu notre clef bootable de l'IUT), puis après l'avoir inséré dans ma machine et lancé le BIOS, je suis allé dans le menu Boot Manager puis j'ai séléctionné le périphérique correspondant à ma clé d'installation. Le GRUB se lance, je choisis donc "Ubuntu", et l'installation de XUbuntu commence, ensuite je peux choisir mes paramètres (Langue, Localisation et autres). Dans le setup de XUbuntu, j'ai pu faire la partition disque pour stocker le système d'exploitation. Ensuite, j'ai rencontré un problème: Même avec tout ça de réalisé, une fois la clé USB enlevée, mon PC ne se lançait pas sur le GRUB. La solution était de changer la priorité de boot, car Windows en premier se lançait automatiquement. En changeant l'ordre de boot, au démarrage désormais, le GRUB se lance et je peux choisir entre Ubuntu et Windows.# OPTION CHOiSIE : UN DUAL-BOOT

# Choix de ma part

En plus des installations obligatoire, j'ai fait deux choix:
- Installer LAMP, qui contient Apache et mysql, pour le developpement de mes projets personnels et pour plus tard dans l'année
- Changer l'aspect graphique de mon environnement, afin de lui donner un ton un peu plus moderne, pour celaj'ai installé le theme "Dracula", les pack d'icones "Tela-purple dark" et j'ai installé comme police par défaut "Roboto Regular"

<h2>Lien de mon git</h2>
<a href="https://gitlab.com/thomas.loque/sae-systeme-1">Ici</a>